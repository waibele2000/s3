| Name                         | Konstante                                     | Nebenbedingung |
| ---------------------------- | --------------------------------------------- | -------------- |
| Eigenleitungsdichte Silizium | $n_i=1,5·10^{10}cm^{-3}$                      | $T=300K$       |
| Temperaturspannung           | $U_T\approx26mV$                              | $T=300K$       |
| Boltzmann-Konstante          | $k_B=1,381·10^{-23}\dfrac{Ws}K$               |                |
| Elementarladung              | $q=1,602·10^{-19}As$                          |                |
| Elektrische Feldkonstante    | $\varepsilon_0=8,854·10^{-14}\dfrac{As}{Vcm}$ |                |
| Permittivität Silizium       | $\varepsilon_{r,Si}=11,9$                     |                |
| Temperaturabhängigkeit       | $E_{g0}=1,206\ eV=1.206·1,602·10^{-19}Ws$     | $T=0K$         |



## 1. pn-Übergang

| Begriff                              | Bedeutung                                                    |
| ------------------------------------ | ------------------------------------------------------------ |
| Halbleiter                           | Elementarhalbleiter (keine Dotierung), Verbindungshalbleiter (Dotierung), Organische Halbleiter |
| Reine Halbleiter                     | nur bei  $T>0K$  leitend                                     |
| Dotieren: Diffusionsofen             | Wafer in 1000°C-Gas mit Dotierstoffen (-grob, +günstig)      |
| Dotieren: Ionenim-plantationsanlagen | Halbleiter unter Ionen-Beschuss (+fein, -teuer)              |
| Emissionskoeffizient n               | Rekombination der Ladungsträger in der Raumladungszone       |



| nicht dotiert                       | p-dotiert                                                    | n-dotiert                                                    | abrupter pn-Übergang                                         |
| ----------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                                     | ![image-20200929160727593](../../Typora.assets/image-20200929160727593.png) | ![image-20200929160713317](../../Typora.assets/image-20200929160713317.png) | ![image-20200929160747999](../../Typora.assets/image-20200929160747999.png) |
| $n_i^2=n·p$                         | $p_p=N_A$    (Akzeptor)                                      | $n_n=N_D$    (Donator)                                       | $U_T=\dfrac{k_B·T}q\\ 25,8mV\ (300K)$                        |
| $n_i=1,5·10^{10}cm^{-3}$ (bei 300K) | $n_p=\dfrac{n_i^2}{p_p}=\dfrac{n_i^2}{N_A}$                  | $p_n=\dfrac{n_i^2}{n_n}=\dfrac{n_i^2}{N_D}$                  | $\varphi_D=U_T\ln\left(\dfrac{N_AN_D}{n_i^2}\right)$         |
|                                     | $d_n=\sqrt{\cfrac{2\varepsilon(\varphi_D+U_R)}{qN_D\big(1+\frac{N_D}{N_A}\big)}}$ | $d_n=\sqrt{\cfrac{2\varepsilon(\varphi_D+U_R)}{qN_A\big(1+\frac{N_A}{N_D}\big)}}$ | $d=d_p+d_n$                                                  |

<img src="../../Typora.assets/image-20200930114858428.png" alt="image-20200930114858428" style="zoom:50%;" />

|        | Raumladungsdichte      | Elektrisches Feld                                            | Potential                                                    |
| ------ | ---------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **p**  | $\dfrac{\rho_p}q=-N_A$ | $E(x)=-\dfrac{qN_A}\varepsilon(x+d_p)$                       | $U(x)=\dfrac{qN_A}\varepsilon\left(\dfrac{x^2}2+d_px+\dfrac{d_p^2}2\right)$ |
| **n**  | $\dfrac{\rho_n}q=-N_D$ | $E(x)=\dfrac{qN_D}\varepsilon(x-d_n)$                        | $U(x)=-\dfrac{qN_D}\varepsilon· \left(\dfrac{x^2}2-d_nx\right)+ \dfrac{qN_Ad_p^2}{2\ \varepsilon}$ |
| **pn** | $d_p·N_A=d_n·N_D$      | $E(0)=-\dfrac{q·N_D·d_n}\varepsilon\\ =-E_{max}=-\dfrac{q·N_A·d_p}\varepsilon$ | $\varphi_D=U(d_n)-U(d_p)\\ =\dfrac{q}{2\varepsilon}(N_D·d_n^2+N_A·d_p^2)$ |
|        |                        | **Äußere Spannung U~R~**<br />**Diffusionspotential** $\varphi_D$ | $\varphi_D+U_R=\dfrac{q}{2\varepsilon}(N_D·d_n^2+N_A·d_p^2)$ |





## 2. Halbleiterdioden 



### 2.1 Einfluss der Flussspannung U~D~

| Löcher in N–Seite                                    | Elektronen in P–Seite                                | Löcher in N–Seite                                  | Elektronen in P–Seite                               |
| ---------------------------------------------------- | ---------------------------------------------------- | -------------------------------------------------- | --------------------------------------------------- |
| $p_{n0}=p_p·e^{-\left(\frac{\varphi_D}{U_T}\right)}$ | $n_{p0}=n_n·e^{-\left(\frac{\varphi_D}{U_T}\right)}$ | $p_n(d_n)=p_{n0}·e^{\left(\frac{U_D}{U_T}\right)}$ | $n_p(-d_p)=n_{p0}·e^{\left(\frac{U_D}{U_T}\right)}$ |

| Löcherstromdichte auf n–Seite                                | Elektronenstromdichte auf p-Seite                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $j_p\bigg\vert_{x=d_n}=\dfrac{q·D_p}{L_p}·p_{n0}\left(e^{\left(\frac{U_D}{U_T}\right)}-1\right)$ | $j_p\bigg\vert_{x=-d_p}=\dfrac{q·D_n}{L_n}·n_{p0}\left(e^{\left(\frac{U_D}{U_T}\right)}-1\right)$ |



### 2.2 Diodenstrom

| Sättigungsstrom                                              | ideale Diodengleichung                           | Temperaturabhängiger Sättigungsstrom |
| ------------------------------------------------------------ | ------------------------------------------------ | ------------------------------------ |
| $I_S=A·q·n_i^2\left(\dfrac{D_p}{L·N_D}+\dfrac{D_n}{L_n·N_A}\right)$ | $I_D=I_S·\left(e^{\frac{U_D}{(n)·U_T}}-1\right)$ | $I_S=c·T^3·e^{-\frac{E_{g0}}{k_BT}}$ |

| Temperaturkoeffizient                      | Temperaturkoeffizient                                        |                                                              |
| ------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $TK_{I_S}=\dfrac3T+\dfrac{E_{g0}}{k_BT^2}$ | $TK_{I_D}=\frac{\part I_D}{\part T}\frac1{I_D}\bigg\vert_{U_D=const}=\dfrac{E_{g0}-q·U_D}{k_BT^2}$ | $\dfrac{\part U_D}{\part T}\bigg\vert_{I_D=const}=\dfrac{U_D-\frac{E_{g0}}q}T$ |
| Temperaturabhängigkeit Sättigungsstrom     | Temperaturabhängigkeit Diodenstrom                           | Temperaturabhängigkeit Diodenspannung                        |



### 2.3 Kleinsignalmodell

| Kleinsignal-Leitwert (Steigung)               | $U_D\le-100mV$ | $U_D\ge100mV$                                                |
| --------------------------------------------- | -------------- | ------------------------------------------------------------ |
| $g_0=\dfrac1{n·U_T}·I_Se^{\frac{U_D}{n·U_T}}$ | $g_0\approx0$  | $g_0=\dfrac{I_D}{n·U_T}\qquad I_D\approx I_S·e^{\frac{U_D}{n·U_T}}$ |

| Diffusionskapazität | Sperrschichtkapazität                                        | Sperrschicht-kapazität (abrupt)                      | nicht abrupt                                                 |
| ------------------- | ------------------------------------------------------------ | ---------------------------------------------------- | ------------------------------------------------------------ |
| $C_D=\tau_D·g_0$    | $C_{j0}=A·\sqrt{\dfrac{q·\varepsilon· N_D N_A}{2(N_A+N_D)·\varphi_D}}$ | $C_j=\dfrac{C_{j0}}{\sqrt{1-\frac{U_D}{\varphi_D}}}$ | $C_j=\dfrac{C_{j0}}{\left(1-\frac{U_D}{\varphi_D}\right)^{m_j}}$ |

​		<img src="../../Typora.assets/image-20201006111904143.png" alt="image-20201006111904143" style="zoom: 40%;" />						<img src="../../Typora.assets/image-20201011190511950.png" alt="image-20201011190511950" style="zoom: 30%;" />



### 2.4 [Zener-Diode](https://www.youtube.com/watch?v=O0ifJ4oVdG4) 

| Allgemeines Ersatzschaltbild                                 |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20201024113303534.png" alt="image-20201024113303534" style="zoom: 67%;" /> | <img src="../../Typora.assets/image-20201024114019126.png" alt="image-20201024114019126" style="zoom: 67%;" /> |
| **Ersatzschaltbild in Schaltung**                            | <img src="../../Typora.assets/image-20201024132131974.png" alt="image-20201024132131974" style="zoom: 25%;" /> |

| Spannungsstabilisierung                                      | Symmetrische Begrenzung Wechselspannung                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20201024134502677.png" alt="image-20201024134502677" style="zoom: 45%;" /> | <img src="../../Typora.assets/image-20201022095232115.png" alt="image-20201022095232115" style="zoom: 50%;" /> |
| $U_L\approx U_{Z0}+\dfrac{r_Z}{R_V}·U_E-r_Z·I_L\qquad I_{Z,max}=\dfrac{P_{V,max}}{U_Z}$ | $\vert U_L\vert\le U_Z+U_D$                                  |
| Einfluss U~E~ auf U~L~ (Prozent)<br />$\dfrac{\part U_L}{\part U_E}\bigg\vert_{I_L=const}=\dfrac{r_Z}{R_V}\qquad\qquad\dfrac{\part U_L}{\part I_L}\bigg\vert_{U_E=const}=-r_Z$ |                                                              |
| Verlustleistung ($r_Z$  vernachlässigen):  $P_V=U_Z·I_Z$     |                                                              |



### 2.5 Kapazitäts-Diode

| $C_D(U_D)=\dfrac{C_{j0}}{\left(\frac{U_D}\varphi+1\right)^{m_j}}\qquad C_{ges}=C_D+C\\f_R=\dfrac1{2\pi\sqrt{L·C_ {ges}}}\approx\dfrac1{2\pi\sqrt{L·(C+C_D(U_S))}}$ | $f_R\approx\dfrac1{2\pi\sqrt{L·(C+0,5·\underbrace{C(U_S)}_{Fkt.})}}$ |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Abstimmbarer Schwingkreis<br />![image-20201024125109043](../../Typora.assets/image-20201024125109043.png) | <img src="../../Typora.assets/image-20201022102608803.png" alt="image-20201022102608803"  /> |



### 2.6 pin-Diode

> Geringer Spannungsabfall in Flußrichtung	+	Hohe Durchbruchspannung in Sperrrichtung

| Leistungsgleichrichter:  R                                   | C    $\boxed{\gamma=\omega t=2\pi f=\dfrac{2\pi}T}$          | R + C                                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20201025092031694](../../Typora.assets/image-20201025092031694.png) | ![image-20201025092052266](../../Typora.assets/image-20201025092052266.png) | ![image-20201025092916202](../../Typora.assets/image-20201025092916202.png) |
| $\overline U_d=\dfrac{\hat U_N}\pi\qquad \hat U_N=\sqrt2·U_N$ | $\overline U_d=\hat U_N=\sqrt2·U_N$                          | $\overline U_d=\hat U_N=\sqrt2·U_N$                          |
| $I_{d,max}=\dfrac{\hat U_N}R$                                | $I_{d,max}=\omega C\hat U_N$                                 | Spitzenspannungsmesser:<br />$\tau_F=\frac1{2\pi RC}$        |
| $\hat U_{Sp}=\hat U_N$                                       | $\hat U_{Sp}=2\hat U_N$                                      | $\hat U_{Sp}=2\hat U_N$                                      |

|                      | Zweipuls-Mittelpunktschaltung (M2)                           | Zweipuls-Brückenschaltung (B2)                               |
| -------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
|                      | ![image-20201025095348356](../../Typora.assets/image-20201025095348356.png) | ![image-20201025095415825](../../Typora.assets/image-20201025095415825.png) |
| $RC\ll\dfrac1{2f_N}$ | $\overline U_d=\dfrac2\pi·\hat U_{S1}=\dfrac{2\sqrt2}\pi·U_{S1}$ | $\overline U_d=\dfrac2\pi·\hat U_{S1}=\dfrac{2\sqrt2}\pi·U_{S1}$ |
| $RC\gg\dfrac1{2f_N}$ | $\overline U_d=\hat U_{S1}\qquad \hat U_{Sp}=2\hat U_{S1}$   | $\overline U_d=\hat U_{S1}\qquad \hat U_{Sp}=\hat U_{S1}$    |

| Zweipuls-Brückenschaltung mit Glättungsinduktivität          | Spannungsverdoppler                                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20201026151903207.png" alt="image-20201026151903207" style="zoom: 33%;" /> | <img src="../../Typora.assets/image-20201026152043167.png" alt="image-20201026152043167" style="zoom: 33%;" /> |
| $\overline U_R=\dfrac2\pi\hat U_{S1}=\dfrac2\pi\hat U_{SP}$  | $\hat U_d=2·\hat U_N$                                        |

​	

### 2.7 Tunnel-Diode



### 2.8 Foto-/Schottky-Diode

| Fotodiode                                                    | Schottky-Diode                                               |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20201026204936048.png" alt="image-20201026204936048" style="zoom:33%;" /> | <img src="../../Typora.assets/image-20201112211759564.png" alt="image-20201112211759564" style="zoom: 33%;" /> |
| $I_D=I_S·\left(e^{\frac{U_D}{n·U_T}}-1\right)-I_L$           | $I_D=I_S·\left(e^{\frac{U_D}{U_T}}-1\right)$                 |



<div style="page-break-after: always; break-after: page;"></div>

## 3. Bipolare Transistoren (S. 89-140)

| Sperrbereich                 | Normal-/Vorwärtsbetrieb                                      | Inversbetrieb                                                | Sättigung                    |
| ---------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ---------------------------- |
| D~E~ sperrt<br />D~C~ sperrt | D~E~ leitet<br />D~C~ sperrt                                 | D~E~ sperrt<br />D~C~ leitet                                 | D~E~ leitet<br />D~C~ leitet |
|                              | $U_{BE}>0\quad U_{BC}<0$                                     | $U_{BE}<0$                                                   | $U_{BE}>0\quad U_{BC}>0$     |
|                              | $\vert U_{BC}\vert \gg U_T~→~e^{\frac{U_{BC}}{U_T}}\approx 0$ | $\vert U_{BC}\vert \gg U_T~→~e^{\frac{U_{BE}}{U_T}}\approx 0$ | $U_{CE}\le U_{BE}$           |

![image-20210201111053174](../../Typora.assets/image-20210201111053174.png)

- Leistungsverstärkung bei Schwingfrequenz:  $1~→~0dB$



### 3.1 Gleichstrommodell: Ebers-Moll-Modell

| npn-Transistor                                               | Injektionsversion                                            | Vorwärtsbetrieb                                              | Rückwärtsbetrieb                                             |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20201025104233926](../../Typora.assets/image-20201025104233926.png) | ![image-20201025104247914](../../Typora.assets/image-20201025104247914.png) | ![image-20201025104307964](../../Typora.assets/image-20201025104307964.png) | ![image-20201025104318299](../../Typora.assets/image-20201025104318299.png) |
| $I_C=A_F·I_F-I_R$                                            | $I_F=I_{ES}·\left(e^{\frac{U_{BE}}{U_T}}-1\right)$           | $A_F=\dfrac{I_C}{-I_E}$                                      | $A_R=\dfrac{I_E}{-I_C}$                                      |
| $I_E=A_R·I_R-I_F$                                            | $I_R=I_{CS}·\left(e^{\frac{U_{BC}}{U_T}}-1\right)$           | $B_F=\dfrac{I_C}{I_B}=\dfrac{A_F}{1-A_F}$                    | $B_R=\dfrac{I_E}{I_B}=\dfrac{A_R}{1-A_R}$                    |
|                                                              | $I_B=-I_C-I_E$                                               | Transport-sättigungsstrom:                                   | $I_S=A_F·I_{ES}\\ \quad=A_R·I_{CS}$                          |

| Variante der Injektions-                                     | version                                                     | Transportversion                                             |                                                              |
| ------------------------------------------------------------ | ----------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20201025104454617](../../Typora.assets/image-20201025104454617.png) | $I_B=-I_C-I_E\\~\\=\dfrac{I_{CC}}{B_F}+\dfrac{I_{EC}}{B_R}$ | ![image-20201025104510472](../../Typora.assets/image-20201025104510472.png) | $I_{CT}=I_{CC}-I_{CE}\\ =I_S\big(e^{\frac{U_{BE}}{U_T}} -e^{\frac{U_{BC}}{U_T}}\big)\\~\\~\\I_B=\dfrac{I_{CC}}{B_F}+\dfrac{I_{EC}}{B_R}$ |
| $I_{CC}=I_S\left(e^{\frac{U_{BE}}{U_T}}-1\right)$            | $I_C=I_{CC}-\dfrac{I_{EC}}{A_R}$                            | $\dfrac{I_{CC}}{B_F}=\dfrac{I_S}{B_F}\left(e^{\frac{U_{BE}}{U_T}}-1\right)$ | $I_C=I_{CT}-\dfrac{I_{EC}}{B_R}$                             |
| $I_{EC}=I_S\left(e^{\frac{U_{BC}}{U_T}}-1\right)$            | $I_E=I_{EC}-\dfrac{I_{CC}}{A_F}$                            | $\dfrac{I_{EC}}{B_R}=\dfrac{I_S}{B_R}\left(e^{\frac{U_{BC}}{U_T}}-1\right)$ | $I_E=-I_{CT}-\dfrac{I_{CC}}{B_F}$                            |



---

$$
I_C=I_S\left(e^{\frac{U_{BE}}{U_T}}-e^{\frac{U_{BC}}{U_T}}\right)-\dfrac{I_S}{B_R}\left(e^{\frac{U_{BC}}{U_T}}-1\right)\qquad\qquad
I_B=\dfrac{I_S}{B_F}\left(e^{\frac{U_{BE}}{U_T}}-1\right)+\dfrac{I_S}{B_R}\left(e^{\frac{U_{BC}}{U_T}}-1\right)
$$

---



### 3.2 Aktiver Betrieb

| Näherung aktiver Vorwärtsbetrieb                             | $U_{BE}>0,\quad U_{CB}>0\\ U_{BE}\gg U_T$ | $I_C=I_S·e^{\frac{U_{BE}}{U_T}}\quad I_B=\dfrac{I_S}{B_F}·\left(e^{\frac{U_{BE}}{U_T}}-1\right)\approx\dfrac{I_C}{B_F}$ |
| ------------------------------------------------------------ | ----------------------------------------- | ------------------------------------------------------------ |
|                                                              | npn:  $N_D\gg N_A,\\ A_F\approx1$         | $I_S=A·q·n_i^2·\dfrac{D_n}{W_B·N_A}\approx I_{ES}$           |
| **mit Early-Effekt**<br />Erhöhung von U~CE~ & U~CB~ und dadurch I~C~ | $U_{CE}\approx U_{CB}$                    | $I_C=I_S·e^{\frac{U_{BE}}{U_T}}\left(1+\dfrac{U_{CB}}{U_A}\right)\approx I_S·e^{\frac{U_{BE}}{U_T}}\left(1+\dfrac{U_{CE}}{U_A}\right)$ |
| **Aktiver Inversbetrieb**                                    | $B_R\approx\dfrac1{100}·B_F$              | $B_F$  (Vorwärts)    /    $B_R$  (Invers)                    |
| **Faktor**                                                   | $A=\dfrac{B}{1+B}$                        | $B=\dfrac{A}{1-A}$                                           |



### 3.3 Kleinsignalmodell

| Steigung (Leitwert)                                          | Kleinsignalstrom                                             | Verstärkung                                      |                             |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------ | --------------------------- |
| $g_m=\dfrac{i_c}{u_{be}}=\frac{dI_C}{dU_{BE}}=\dfrac{I_C}{U_T}$ | $i_c=\dfrac{I_C}{U_T}·u_{be}\qquad i_C=I_C·e^{\frac{u_{be}}{U_T}}$ | $\beta=\dfrac{i_c}{i_b}\quad B=\dfrac{I_C}{I_B}$ | $r_\mu=\dfrac{u_{ce}}{i_b}$ |
| $u_{be}\ll U_T$                                              | $U_{be}\ll U_T$                                              | $\beta=B\quad(B\ const)$                         | $r_\mu\ge r_o·\beta$        |

| Eingangswiderstand                             | Basis-Diffusionskapazität                                    | Ausganswiderstand                                            |
| ---------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $r_\pi=\dfrac{u_{be}}{i_b}=\dfrac{\beta}{g_m}$ | $C_D=\dfrac{q_L}{u_{be}}=\dfrac{\Delta Q_L}{\Delta U_{BE}}=\tau_F·g_m$ | $r_o=\dfrac{u_{ce}}{i_c}=\dfrac{U_A}{I_C}=\dfrac{U_T}{\eta·I_C}=\dfrac1{\eta·g_m}=\dfrac1{g_o}$ |

<img src="../../Typora.assets/image-20201104095907861.png" alt="image-20201104095907861" style="zoom: 33%;" />			$I_E=\dfrac{1+\beta}\beta·I_C$



### 3.4 Vollständiges Kleinsignal-Ersatzschaltbild

![image-20201109184825237](../../Typora.assets/image-20201109184825237.png)



### 3.5 Transitfrequenz

| Transitfrequenz  f~T~                        | Frequenzgang                                                 | Amplitudengang                                               |
| -------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\beta(j\omega)=\dfrac{i_c}{i_b}\overset!=1$ | $\beta(j\omega)=\dfrac{g_m·r_\pi}{1+j\frac\omega{\omega_\beta}}$ | $\vert\beta(j\omega)\vert=\dfrac{g_m·r_\pi}{\sqrt{1+(\frac\omega{\omega_\beta})^2}}$ |
| $f=f_T$                                      | $\omega_\beta=\dfrac1{r_\pi·(C_\pi+C_\mu)}$                  | $\vert\beta(j\omega)\vert_{dB}=\left[20\log(\beta)-20\log\left( \sqrt{1+(\frac\omega{\omega_\beta})^2}\right)\right]\text{dB}$ |

| Tiefe Frequenzen                                        | Hohe Frequenzen                                              |                                                 |
| ------------------------------------------------------- | ------------------------------------------------------------ | ----------------------------------------------- |
| $\vert\beta(j\omega)\vert_{dB}=20\log(\beta)\text{ dB}$ | $\vert\beta(j\omega)\vert_{dB}=\left[20\log(\beta)-20\log\left( \frac\omega{\omega_\beta}\right)\right]\text{dB}$ | $f_T=\dfrac{\omega_T}{2\pi}$    Transitfrequenz |
| $\omega<\omega_\beta$                                   | $\omega>\omega_\beta$                                        | $\omega_T$    Transit(kreis)frequenz            |
| horizontale Gerade                                      | Gerade mit Steigung  **–20dB/Dekade**                        | $\omega_\beta$    β-Grenzfrequenz               |

<img src="../../Typora.assets/image-20201104113729409.png" alt="image-20201104113729409" style="zoom:40%;" />

| Optimale Leistungsverstärkung                  | Maximale Schwingfrequenz                           |
| ---------------------------------------------- | -------------------------------------------------- |
| $V_{opt}=\dfrac{g_m}{4r_BC_DC_\mu·(2\pi f)^2}$ | $f_{max}=\sqrt{\dfrac{f_{T,max}}{8\pi·r_B·C_\mu}}$ |

| Übersteuerungsgrad                  | Ungesättigter Transistor Inverter                            |
| ----------------------------------- | ------------------------------------------------------------ |
| $\ddot u=\dfrac{i_B}{I_{B\ddot u}}$ | $U_{CE,min}=U_{BE,ON}-U_{SD}\approx700mV-300mV=400mV>U_{CE,sat}$ |





## 4. Feldeffekt-Transistoren

- wenn Bulk und Source kurzgeschlossen sind, dann gibt es kein  $g_{mb}$
  → dadurch kein Bulk-Effekt und keine zusätzliche Stromquelle in ESB erforderlich



### 4.1 Übersicht

<img src="../../Typora.assets/image-20201125122735969.png" alt="image-20201125122735969" style="zoom: 50%;" />

---

<img src="../../Typora.assets/image-20201125131114049.png" alt="image-20201125131114049" style="zoom: 33%;" />			<img src="../../Typora.assets/image-20201125131149130.png" alt="image-20201125131149130" style="zoom: 33%;" />

|                       | JFET        | E-MOSFET    | D-MOSFET                |
| --------------------- | ----------- | ----------- | ----------------------- |
| **mögliche Zustände** | depletion   | enhancement | depletion / enhancement |
| **default (U~GS~=0)** | Kurzschluss | Leerlauf    | Kurzschluss /           |



### 4.2 MOSFET - Statisches Kleinsignalmodell

<img src="../../Typora.assets/image-20201125131851055.png" alt="image-20201125131851055" style="zoom: 33%;" />

<img src="../../Typora.assets/image-20201125132507634.png" alt="image-20201125132507634" style="zoom: 33%;" />

| Steilheit                                                    | Ausgangs-widerstand              | Bulk-Stromquelle                                    | $\gamma_n=\dfrac{\sqrt{2·\varepsilon_0 \varepsilon_r·q·N_A}}{C_{OX}}$ |
| ------------------------------------------------------------ | -------------------------------- | --------------------------------------------------- | ------------------------------------------------------------ |
| $g_m\approx\sqrt{2k_n·\dfrac WL·I_D}\\=\sqrt{2k_n\dfrac WLI_D(1+\lambda U_{DS})}$ | $r_o\approx\dfrac1{\lambda·I_D}$ | $g_{mB}=\dfrac{\gamma·g_m}{2·\sqrt{\Phi_F-U_{BS}}}$ | $\gamma_p=\dfrac{\sqrt{2·\varepsilon_0 \varepsilon_r·q·N_D}}{C_{OX}}$ |

| Linearer Bereich    $U_{DS}\le(U_{GS}-U_{TH})$               | Sättigungsbereich    $U_{DS}\ge(U_{GS}-U_{TH})$              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $I_D=k'·\dfrac WL·\left[(U_{GS}-U_{TH})·U_{DS}-\dfrac12·U_{DS}^2\right]$ | $I_D=\dfrac{k'}2·\dfrac WL·(U_{GS}-U_{TH})^2·(1+\lambda·U_{DS})$ |



### 4.3 MOSFET als Schalter

| Anstiegszeit                                               | Abfallzeit                                                   |
| ---------------------------------------------------------- | ------------------------------------------------------------ |
| $t_R=R_LC_L\ln\left(\dfrac{U_{DD}-U_L}{U_{DD}-u_o}\right)$ | $t_F\approx\dfrac{C_L}{k'\frac WL(U_H-U_{TH})}\left(\dfrac{2U_{TH}}{U_H-U_{TH}}+\ln\left(\dfrac{2(U_H-U_{TH})-u_o}{u_o}\right)\right)$ |
| $R_{ON}\approx\dfrac1{k'·\frac WL·(U_{GS}-U_{TH})}$        | $\Delta u_o=-\left(\dfrac{C_{GD}}{C_{GD}+C_L}\right)·(u_i+U_{TH})$ |

<div style="page-break-after: always; break-after: page;"></div>

## 5. Elementare Verstärkerschaltungen

|                               | Bipolar Transistor                                           | MOS FET                                       |
| ----------------------------- | ------------------------------------------------------------ | --------------------------------------------- |
| **Typischer Betriebszustand** | aktiver Vorwärtsbetrieb<br />$U_{BE}>0\qquad U_{CB}\ge0\qquad\vert U_{BC}\vert\gg U_T$ | Sättigungsbereich<br />$U_{GS}-U_{TH}<U_{DS}$ |



### 5.1 [Emitterschaltung](https://youtu.be/P2Z5-U9g8vs)

| Bezeichnung<br />                                            | Aufbau<br />$R_i=\dfrac{u_i}{i_i}\qquad R_o=\dfrac{u_o}{i_o}$ | Kleinsignalersatzschaltbild<br />$V_u=\dfrac{u_o}{u_i}\bigg\vert_{i_o=0}\qquad V_i=\dfrac{i_o}{i_i}\bigg\vert_{u_o=0}$ |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Emitterschaltung**<br /><br />$U_o=U_{CC}-R_CI_Se^{\frac{U_i}{U_T}}$ | <img src="../../Typora.assets/image-20201126082858586.png?lastModify=1606378360" alt="image-20201126082858586" style="zoom:50%;" /> | ![image-20201126100042742](../../Typora.assets/image-20201126100042742.png)<br />![image-20201126083003386](../../Typora.assets/image-20201126083003386.png) |
| $S=g_m$                                                      | $R_i=r_\pi\qquad R_o=r_o\parallel R_C$                       | $V_u=-g_m(r_o\parallel R_C)\qquad V_i=g_mr_\pi=\beta$        |
| **Emitterschaltung<br /><br /> Wechselstrom-<br />Gegenkopplung** | <img src="../../Typora.assets/image-20201126083852402.png?lastModify=1606378360" alt="image-20201126083852402" style="zoom:50%;" /> | ![image-20201126083908679](../../Typora.assets/image-20201126083908679.png) |
| $S=\dfrac{g_m}{1+g_mR_E}\\R_o'=r_o(1+g_mR_E)$                | $R_i\approx r_\pi(1+g_mR_E)\\~\\ R_o=R_o'\parallel R_C$      | $V_u=-\dfrac{g_mR_C}{1+g_mR_E}\overset{starke\\Gegen-\\kopplung}=-\dfrac{R_C}{R_E}\qquad V_i=\beta$ |
| **Miller-Approximation für Hochfrequenz**                    | ![image-20201126091226170](../../Typora.assets/image-20201126091226170.png?lastModify=1606378360) | ![image-20201126091249227](../../Typora.assets/image-20201126091249227.png?lastModify=1606378360)<br />![image-20201126093825547](../../Typora.assets/image-20201126093825547.png) |
| $\omega_0=\dfrac{R+r_B+r_\pi}{C_tr_\pi(R_S+r_B)}$            | $\omega_0=\left(1+\frac{r_\pi}{R_S+r_B}\right)\frac{2\pi f_{T,max}}{\beta}\\~\\~\\C_t=C_\pi+C_\mu(1+g_mR_C)$ | $V_U(j\omega)=\underbrace{\dfrac{-g_mR_C·r_\pi}{R_S+r_B+r_\pi}}_{V_U(0)}·\dfrac1{1+j\frac\omega{\omega_0}}\\~\\C_M=(1+g_mR_C)·C_\mu=(1+\vert V_u\vert)·C_\mu$ |

<div style="page-break-after: always; break-after: page;"></div>

### 5.2 [Basisschaltung](https://youtu.be/a1rOS3SXug0)

| $S=-\dfrac{g_m}{1+\frac{r_B}{r_\pi}}$                        | $R_i=r_e(1+\frac{r_B}{r_\pi})\qquad R_o=R_C$                 | $V_U=-R_oS=\dfrac{g_mR_C}{1+\frac{r_B}{r_\pi}}\qquad V_I=-\alpha$ |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Basisschaltung**<br /><br />$U_o=U_{CC}-R_CI_Se^{-\frac{U_i}{U_T}}\\~\\\dfrac1\alpha=1+\dfrac1\beta\quad r_e=\dfrac\alpha{g_m}$ | ![image-20201126095349362](../../Typora.assets/image-20201126095349362.png) | ![image-20201126101511818](../../Typora.assets/image-20201126101511818.png) |

### 5.3 Kollektorschaltung (Emitterfolger)

| $V_i=-\dfrac{1+\beta}{1+\frac{R_L}{r_o}}\overset{R_L\ll r_o}\approx-(1+\beta)$ | $V_U=\dfrac{u_o}{u_s}=\dfrac{1}{1+\frac{R_S+r_\pi}{(1+\beta)(R_L\parallel r_o)}}\overset{R_S\ll r_\pi\\R_L\ll r_o\\\beta\ll1}=\dfrac{g_mR_L}{1+g_mR_L}\overset{R_L\gt\\r_o→\infty}\approx1$ |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20201126102650655.png" alt="image-20201126102650655" style="zoom:50%;" />      $\alpha=\dfrac\beta{1+\beta}$ | ![image-20201126102629071](../../Typora.assets/image-20201126102629071.png) |
| $R_i=r_\pi+R_L·\dfrac{1+\beta}{1+\frac{R_L}{r_o}}$           | $R_i\overset{R_L\ll r_o}\approx r_\pi+R_L(1+\beta)\overset{\beta=r_\pi g_m\gg1}\approx r_\pi(1+g_mR_L)$ |
| $R_o=\dfrac1{\frac{1+\beta}{R_S+r_\pi}+\frac1{r_o}}$         | $R_o\overset{r_o→\infty}\approx \dfrac{R_S+r_\pi}{1+\beta}\approx\dfrac{R_s}{1+\beta}+\dfrac{\alpha}{g_m}$ |



### 5.4 Schaltungen mit Feldeffektransistoren

- durch Gate fließt kein Strom
- Standartmäßig im Sättigungsbereich

| Grossignal (DC)              | Kleinsignal (AC)                                             |
| ---------------------------- | ------------------------------------------------------------ |
| Kondensator wird zu Leerlauf | - alle hohen Spannungsquellen werden mit Ground kurzgeschlossen<br />- Kondensator wird zu Kurzschluss |

| Sperrbereich | Sättigungsbereich                              | linearer Bereich    |
| ------------ | ---------------------------------------------- | ------------------- |
|              | $U_i-U_{TH}<U_o\qquad U_{GS}-U_{TH}\le U_{DS}$ | $U_o\le U_i-U_{TH}$ |

<div style="page-break-after: always; break-after: page;"></div>

### 5.5 [Source-Schaltung](https://youtu.be/h29TEPu1Bn8) ([Englisch](https://youtu.be/aeiHtgBbMW4))

| ![image-20201207122556501](../../Typora.assets/image-20201207122556501.png?lastModify=1612020847) | ![image-20201207122725048](../../Typora.assets/image-20201207122725048.png?lastModify=1612020847) |                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------ |
| **Steilheit**                                                | $S=g_m\approx\sqrt{2k_n·\dfrac WL·(1+\lambda U_{DS})·I_D}$   | $S=g_m=\dfrac{i_o}{u_i}\bigg\vert_{u_o=0}$ |
| **Verstärkung**                                              | $V_u=\dfrac{u_o}{u_i}\bigg\vert_{i_o=0}=-g_m·R_o$            | $V_i=\dfrac{i_o}{i_i}\bigg\vert_{u_o=0}$   |
| **Sättigungsbereich**                                        | $I_D=\dfrac{k'·W}{2·L}·(U_i-U_{TH})^2·(1+\lambda U_{DS})$    | $U_o=U_{DD}-R_D·I_D(U_i)$                  |
| **Widerstand**                                               | $R_o=\dfrac{u_o}{i_o}=r_o\parallel R_D\qquad\qquad r_o=\dfrac1{\lambda I_D}$ | $R_i=\dfrac{u_i}{i_i}=\infty$              |



### 5.6 Source-Schaltung mit Wechselstrom-Gegenkopplung

| ![image-20201217125821484](../../Typora.assets/image-20201217125821484.png?lastModify=1612020847) | ![image-20201217125849128](../../Typora.assets/image-20201217125849128.png?lastModify=1612020847) |      |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---- |
| Kurzschluss-Steilheit                                        | $S=\dfrac{g_m}{1+R_S(g_m+g_{mb})+\frac{R_S}{r_o}}\overset{r_o\gg R_S}\approx \dfrac{g_m}{1+R_S(g_m+g_{mb})}$ | ———  |
| Eingangswiderstand                                           | $R_i=\infty$                                                 |      |
| Ausgangswiderstand                                           | $R_o=\bigg(r_o·\big[1+R_S(g_m+g_{mb})\big]\bigg)\parallel R_D$    (meistens $R_o\approx R_D$) |      |
| Spannungsverstärkung                                         | $V_u\overset{R_o\approx R_D}=-\dfrac{g_mR_D}{1+R_S(g_m+g_{mb})}\overset{R_S(g_m+g_{mb}\gg1)}=-\dfrac{R_D}{R_S(1+\frac{g_{mb}}{g_m})}$ |      |
| Stromverstärkung                                             | $V_i=V_u·\dfrac{R_i}{R_o}=\infty$                            |      |

<div style="page-break-after: always; break-after: page;"></div>

### 5.7 Miller-Approximation für Hochfrequenz

| ![image-20201217131028276](../../Typora.assets/image-20201217131028276.png?lastModify=1612020847) | ![image-20201217131126654](../../Typora.assets/image-20201217131126654.png?lastModify=1612020847) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $V_U(j\omega)=\dfrac{V_U(0)}{1+j\frac{\omega}{\omega_0}}\qquad \omega_0=\dfrac1{R_SC_t}$ | $C_t=C_{gs}+C_{gd}(1+g_mR_D)$                                |
| $V_U(0)=-g_mR_D$                                             | $C_t=C_{gs}+C_M$                                             |



### 5.8 Gate-Schaltung

| **Bulk / Steilheit**                                         | **Widerstand                        Kurzschluss-Verstärkung** |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $U_B\ge\vert U_i\vert_{max}\qquad u_{gs}=u_{bs}=-u_i$        | $R_i=\dfrac{u_i}{i_i}=\dfrac1{g_m+g_{mb}}\qquad V_u=\dfrac{u_o}{u_i}\bigg\vert_{i_o=0}=R_D (g_m+g_{mb})$ |
| $S=\dfrac{i_o}{u_i}\bigg\vert_{u_o=0}=-(g_m+g_{mb})$         | $R_o=\dfrac{u_o}{i_o}=R_D\qquad\qquad V_i=\dfrac{i_o}{i_i}\bigg\vert_{u_o=0}=-1$ |
| <img src="../../Typora.assets/image-20201217164908339.png?lastModify=1612020847" alt="image-20201217164908339" style="zoom:50%;" /> | <img src="../../Typora.assets/image-20201217165035327.png?lastModify=1612020847" alt="image-20201217165035327" style="zoom:50%;" /> |



### 5.9 Drain-Schaltung (Source-Folger)

| ![image-20201217165825568](../../Typora.assets/image-20201217165825568.png?lastModify=1612020847) | ![image-20201217165929254](../../Typora.assets/image-20201217165929254.png?lastModify=1612020847) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Widerstand**    $R_i=\infty$                               | $R_o=\dfrac1{\frac1{r_o}+g_m+g_{mb}}\overset{r_o→\infty}\approx \dfrac1{g_m+g_{mb}}$ |
| **Verstärkung**    $V_i=\infty$                              | $V_u=\dfrac{g_mr_o}{1+r_o(g_m+g_{mb})+\frac{r_o}{R_L}}\overset{R_L→\infty}\approx \dfrac{g_mr_o}{1+r_o(g_m+g_{mb})}\overset{R_L→\infty\\r_o→\infty}\approx \dfrac1{1+\frac{g_{mb}}{g_m}}$ |

<div style="page-break-after: always; break-after: page;"></div>

## 6. Aktive-Zwei-Transistorschaltungen (256-290)



### 6.1 Darlington-Schaltung

| <img src="../../Typora.assets/image-20201217171350087.png" style="zoom: 40%;" /> | <img src="../../Typora.assets/image-20201217171409456.png" style="zoom: 45%;" /> |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20201217171426565.png" style="zoom: 50%;" /> | <img src="../../Typora.assets/image-20201217171444629.png" style="zoom: 50%;" /> |
| $R_i\approx r_\pi+R_L(1+\beta)$                              | $r_o^*=r_{o2}\qquad g_m^*=\dfrac{g_{m2}}{1+\frac{r_{\pi1}}{r_{\pi2}(1+\beta)}}\overset{I_Q=0}=\dfrac{g_{m2}}2$ |
| $r_\pi^*\approx r_{\pi1}+r_{\pi2}(1+\beta)$                  | $\beta^*=\beta(\beta+1)$                                     |



### 6.2 Kaskode-Schaltung

<img src="../../Typora.assets/image-20201217172103478.png" alt="image-20201217172103478" style="zoom:50%;" />			<img src="../../Typora.assets/image-20201217172123979.png" alt="image-20201217172123979" style="zoom:50%;" />			<img src="../../Typora.assets/image-20201217172143015.png" alt="image-20201217172143015" style="zoom:50%;" />



### 6.3 Bipolar-Differenzverstärker – Großsignal

- Verstärker begrenzt symmetrisch ohne Sättigung
- $U_{id} = 0$  am Eingang liefert  $U_{od} = 0$  am Ausgang (inhärent offsetfrei, unabhängig von  $I_{EE}$ ,  $E_C$ ,  $U_i$  und  $U_o$ )
- Differenzverstärker lassen sich DC-gekoppelt in Serie schalten

$I_{EE}=\dfrac{I_{C1}+I_{C2}}{A}=\dfrac{I_{C1}+I_{C2}}{B}·(B+1)$

| Differenz-Verstärkung                                        | Gleichtakt-Verstärkung                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20201218092634394.png" alt="image-20201218092634394" style="zoom:50%;" /> | <img src="../../Typora.assets/image-20201218093933436.png" alt="image-20201218093933436" style="zoom:50%;" /> |
| <img src="../../Typora.assets/image-20201218093355606.png" alt="image-20201218093355606" style="zoom: 50%;" /> | <img src="../../Typora.assets/image-20201218094041942.png" alt="image-20201218094041942" style="zoom:50%;" /> |
| $U_{od}=A·I_{EE}·R_C·\tanh\left(-\dfrac{U_{id}}{2U_T}\right)$ | $CMRR=\dfrac{V_d}{V_gl}=1+2~g_mR_{EE}$                       |
| $V_d=-g_m·R_C=-\dfrac{I_{EE}R_CA}{2~U_T}\\\overset{U_A<\infty}=-\dfrac{I_{EE}A}{2~U_T}·(R_C\parallel r_o)$ | $V_{gl}=-\dfrac{g_mR_C}{1+2~g_mR_{EE}}\overset{2g_mR_{EE}\gg1}\approx-\dfrac{R_C}{2~R_{EE}}$ |
| $R_{od}=2·(R_C\parallel r_o)\qquad R_{id}=2·r_\pi$           | $R_{igl}=r_\pi+2R_{EE}(\beta+1)\approx 2\beta R_{EE}$        |
| <img src="../../Typora.assets/image-20201218095336676.png" alt="image-20201218095336676" style="zoom:50%;" />    π-Ersatzschaltbild | <img src="../../Typora.assets/image-20201218095347677.png" alt="image-20201218095347677" style="zoom:50%;" />    T-Ersatzschaltbild |

 

### 6.4 Bipolar-Differenzverstärker – Wechelstromgegenkopplung

- Bereich der Eingangsdifferenzspannung, in dem das Signal linear übertragen wird erhöht sich von ± 2U~T~ auf ± (2U~T~ + I~EE~ ⋅ R~E~)

| Dfferenz-Verstärkung                                         | Gleichtakt-Verstärkung                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20201218100457445.png" alt="image-20201218100457445" style="zoom:50%;" />               <img src="../../Typora.assets/image-20201218100521239.png" alt="image-20201218100521239" style="zoom:50%;" /> | <img src="../../Typora.assets/image-20201218100543188.png" alt="image-20201218100543188" style="zoom:50%;" /> |
| $g_m=\dfrac{I_{EE}A}{2~U_T}=\dfrac{I_C}{U_T}\qquad V_d=-\dfrac{g_mR_C}{1+g_mR_E}\overset{g_m·R_E\gg1}\approx-\dfrac{R_C}{R_E}$ | $V_{gl}=-\dfrac{g_mR_C}{1+2~g_mR_{EE}}$                      |
| $CMRR=\dfrac{V_d}{V_{gl}}=\dfrac{2~g_mR_{EE}}{1+g_mR_E}\overset{g_mR_E\gg1}\approx\dfrac{2R_{EE}}{R_E}$ | $V_{gl}\overset{2~g_mR_{EE}\gg1}\approx -\dfrac{R_C}{2~R_{EE}}$ |
| $R_{od}=2(R_C\parallel r_o)\qquad R_{id}=2~r_\pi(1+g_mR_E)$  | $R_{igl}\approx 2\beta R_{EE}$                               |



### 6.5 MOS-Differenzverstärker

<img src="../../Typora.assets/image-20201218100858732.png" alt="image-20201218100858732" style="zoom:50%;" />

##### Großsignalverhalten

| $I_{D1,D2}=\dfrac{I_{SS}}2\pm\dfrac{k'U_{id}·W}{4·L}\sqrt{\dfrac{4~I_{SS}·L}{k'·W}-U_{id}^2}$ | $U_{od}=-\dfrac{k'R_DU_{id}·W}{2·L}·\sqrt{\dfrac{4~I_{SS}·L}{k'·W}-U_{id}^2}$ |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\big\vert U_{id}\big\vert_{max}=\sqrt{\dfrac{2~I_{SS}·L}{k'·W}}$ | $\big\vert U_{id,lin}\big\vert_{max}=\sqrt{\dfrac{I_{SS}·L}{k'·W}}$ |

##### Kleinsignalverhalten

| $V_d=-g_mR_D\overset{\lambda>0}=-g_m(R_D\parallel r_o)$ | $V_{gl}\approx-\dfrac{R_D}{2~R_{SS}}$ | $CMRR\approx 2~g_mR_{SS}=\dfrac{V_d}{V_{gl}}$ |
| ------------------------------------------------------- | ------------------------------------- | --------------------------------------------- |
| $R_{id}=\infty\qquad R_o=2(R_D\parallel r_o)$           | $R_{igl}=\infty$                      |                                               |



## 7. Leistungsverstärker (291-304)

| Klasse A - Eintakt | Klasse B - Gegentakt | Klasse AB - Gegentakt | Klasse D          |
| ------------------ | -------------------- | --------------------- | ----------------- |
| $\eta_{max}=25\%$  |                      | $\eta_{max}=80\%$     | $\eta_{max}>90\%$ |



## 8. Extra

<img src="../../Typora.assets/image-20210130162806759.png" alt="image-20210130162806759" style="zoom: 33%;" />

| Quelle / Widerstand                                          | Ersatzspannungsquelle                                        | Ersatzstromquelle                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20200112152835816.png" alt="image-20200112152835816" style="zoom: 33%;" /> <img src="../../Typora.assets/image-20200112152915915.png" alt="image-20200112152915915" style="zoom: 40%;" /> | <img src="../../Typora.assets/image-20200112192526267.png" alt="image-20200112192526267" style="zoom:40%;" />   <img src="../../Typora.assets/image-20200112171742500.png" alt="image-20200112171742500" style="zoom: 40%;" /> | <img src="../../Typora.assets/image-20200112192601937.png" alt="image-20200112192601937" style="zoom:40%;" />  <img src="../../Typora.assets/image-20200112171936204.png" alt="image-20200112171936204" style="zoom:40%;" /> |
| U + I entgegengesetzt<br />U + I gleich gerichtet            | Leerlaufspannung:  $U_{01}=U_2$<br />Quelle kurzschließen:  $R_{01}=R_1\parallel R_2$ | Kurzschlussstrom:  $I_{01}=I_1$<br />Quelle zu Leerlauf:  $R_{01}=R_1+R_2$ |



## Klausur

1. Aufgabe:  20 Verständnisaufgaben: z.B. Wort, Formel
3. Source-Schaltung mit Gleichstromgegenkopplung (5-06 Emitter-Bipolar / 4-01 / 4-02) :heavy_check_mark:
4. HF Verstärker mit Bipolartransistor (5-07) :heavy_check_mark:
5. Bipolardifferenzverstärker mit Emittergegenkopplung (6-03) :heavy_check_mark:



## Häufige Fehler

- Prüfung auf z.B. <u>Sättigungsbereich</u> nicht vergessen
- Kleinsignal-ESB: alle möglichen Ströme und Spannungen einzeichnen!
- Bei Transistor-Netzwerk, was von Standard-Aufbau abweicht immer Verstärkung selbst herleiten!
- dabei auf Richtung des Stroms der Stromquelle achten:    $u_o={\color{red}{\bold-}}g_m·…$
- Emitterstrom oft $\frac{B+1}{B}$-faches des Kollektorstroms
- eventuell muss C~D~ und r~µ~ ausgerechnet werden?  (Aufgabe 5-7)