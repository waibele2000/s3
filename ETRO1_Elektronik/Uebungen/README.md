# Offizielle Übungen
Unter den folgenden Links findest du veröffentlichte Übungen:
[THU Laufwerk] (https://fs.hs-ulm.de/public/schmidt/Elektronik_I/2_Übungen)

Unter den folgenden Links findest du veröffentlichte Musterlösungen:
[THU Laufwerk] (https://fs.hs-ulm.de/public/schmidt/Elektronik_I/3_Musterlösungen)