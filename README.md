# 3. Semester
## Inhalt
1. [ETRO1_Elektronik](https://gitlab.com/thulm/et/s3/-/tree/main/ETRO1_Elektronik)
2. [SYTH_Systemtheorie](https://gitlab.com/thulm/et/s3/-/tree/main/SYTH_Systemtheorie)
3. [PROGCP_Programmieren_CPP](https://gitlab.com/thulm/et/s3/-/tree/main/PROGCP_Programmieren_CPP)
4. [MAET_Mathematik](https://gitlab.com/thulm/et/s3/-/tree/main/MAET_Mathematik)

Für alle anderen Infos bitte die [README.md](https://gitlab.com/thulm/et/s1/-/blob/main/README.md) aus dem 1. Semester lesen!